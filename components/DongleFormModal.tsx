import React, {useEffect, useState} from "react";
import {TDongle} from "../models/TDongle";
import {Button, Form, FormGroup, Input, Label, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";

export type DongleFormModalProps = {
    dongle: TDongle;
    visible: boolean;
    onClose: () => void;
    onSave: (newId: number) => void;
}

export const DongleFormModal: React.FC<DongleFormModalProps> = ({dongle, visible, onClose, onSave}) => {

    const [id, setId] = useState<string>(`${dongle?.id ?? ''}`);

    useEffect(() => {
        setId(`${dongle?.id ?? ''}`);
    }, [dongle])

    if (!dongle)
        return null;


    return (
        <Modal isOpen={visible} toggle={() => onClose()}>
            <Form
                onSubmit={() => {
                    onSave?.(parseInt(id));
                }}
            >
                <ModalHeader>
                    <h1>Dongle #${dongle.id}</h1>
                </ModalHeader>
                <ModalBody>

                    <FormGroup>
                        <Label>Identifiant</Label>
                        <Input value={id} onChange={(e) => setId(e.target.value)}/>
                    </FormGroup>

                </ModalBody>
                <ModalFooter>
                    <Button type={"submit"}>Sauvegarder</Button>
                </ModalFooter>
            </Form>
        </Modal>
    )
}