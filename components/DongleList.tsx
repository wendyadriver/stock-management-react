import React, { useState} from "react";
import {Badge, Button, Table} from "reactstrap";
import {useDongleFilter} from "../hooks/useDongleFilter";
import {ListPagination} from "./ListPagination";
import {useDongleReducerSelector} from "../reducers/DongleReducer/useDongleReducerSelector";

export type DongleListProps = {}
// @ts-ignore
import {Feather} from "react-web-vector-icons";
import moment from "moment";
import {TDongle} from "../models/TDongle";
import {DongleFormModal} from "./DongleFormModal";

export const DongleList: React.FC<DongleListProps> = () => {
    const filter = useDongleFilter();
    const items = useDongleReducerSelector(({items}) => items);
    const data = items?.[filter] ?? [];
    const [dataList, setDataList] = useState(data);

    const [currentDongle, setCurrentDongle] = useState<TDongle>();
    const onTrash = (itemToDelete: TDongle) => () => {
        if (window.confirm('Voulez-vous vraiment supprimer cet item?')) {
            setDataList(dataList.filter((item) => item.id !== itemToDelete.id));

            //api.deleteDongle(itemToDelete.id);
        }
    }

    const onEditPress = (item: TDongle) => () => {
        setCurrentDongle(item);
    }

    const onAddPress = () => {
        setCurrentDongle({} as TDongle)
    }

    const onIdChange = (id: number) => {
        if (!currentDongle?.id) {
            setDataList([
                ...dataList,
                {
                    id
                } as TDongle
            ]);

            //api.createDongle(data);
        } else {
            setDataList(
                dataList.map((item) => {
                    if (item === currentDongle) {
                        return {
                            ...currentDongle,
                            id
                        }
                    } else {
                        return item;
                    }
                })
            );

            //api.updatedongle(dongle.id, data);
        }
        setCurrentDongle(undefined);

        //api.postData(....);
    }
    return (
        <div className={"mt-4"}>
            <div className={"text-right mb-2"}>
                <Button color={"primary"} size={"xs"} onClick={onAddPress}>Ajouter un dongle</Button>
            </div>
            <Table striped={!!data?.length}>
                <thead>
                <tr>
                    <th>
                        Numéro d'identification
                    </th>
                    <th>
                        Date d'ajout
                    </th>
                    <th>
                        Status
                    </th>
                    <th>
                        Test
                    </th>
                    <th className={"text-center"}>
                        Actions
                    </th>
                </tr>
                </thead>

                <tbody>
                {!dataList.length &&
                <tr>
                    <td colSpan={4} className={"text-center p-4 text-muted"}>Pas de dongle disponible</td>
                </tr>
                }

                {dataList.map((item, index) => (
                    <tr key={`${index}`}>
                        <td>
                            Dongle #{item.id}
                        </td>
                        <td>
                            {moment().format('LLL')}
                        </td>
                        <td>
                            <Badge color={"success"}>Actif</Badge>
                        </td>
                        <td>
                            test
                        </td>
                        <td className={"w-auto text-center"}>
                            <Button size={"sm"} onClick={onEditPress(item)}>
                                <Feather name={"edit"} size={18} color={"#FFF"}/>
                            </Button>
                            {'\u0090'}
                            <Button color={"danger"} size={"sm"} onClick={onTrash(item)}>
                                <Feather name={"trash"} size={18} color={"#FFF"}/>
                            </Button>
                        </td>
                    </tr>
                ))}
                </tbody>
            </Table>

            <div className={"mt-4"}>
                <ListPagination nbPages={10} onChange={() => {

                }}/>
            </div>

            <DongleFormModal
                visible={!!currentDongle}
                onClose={() => setCurrentDongle(undefined)}
                dongle={currentDongle!}
                onSave={onIdChange}
            />
        </div>
    )
}