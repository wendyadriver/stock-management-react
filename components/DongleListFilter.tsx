import React from "react";
import {Badge, ListGroup, ListGroupItem, Nav, NavItem, NavLink} from "reactstrap";
import {useDongleFilter} from "../hooks/useDongleFilter";
import {useDongleReducerSelector} from "../reducers/DongleReducer/useDongleReducerSelector";
import {DongleListFilterItem} from "./DongleListFilterItem";

export type DongleListFilterProps = {}

export const DongleListFilter: React.FC<DongleListFilterProps> = () => {
    const filter = useDongleFilter();
    const nbOngoing = useDongleReducerSelector(({items: {ongoing}}) => ongoing?.length ?? 0);
    const nbFail = useDongleReducerSelector(({items: {fail}}) => fail?.length ?? 0);
    const nbAvailable = useDongleReducerSelector(({items: {available}}) => available?.length ?? 0);
    return (
        <Nav pills color={"primary"} vertical>
            <DongleListFilterItem label={"Item 1"} filter={"ongoing"} count={nbOngoing} active={filter === 'ongoing'}/>
            <DongleListFilterItem label={"Item 2"} filter={"fail"} count={nbFail} active={filter === 'fail'}/>
            <DongleListFilterItem label={"Item 3"} filter={"available"} count={nbAvailable}
                                  active={filter === 'available'}/>
            <DongleListFilterItem label={"Item 4"} filter={"item4"} count={nbAvailable}
                                  active={filter === 'item4'}/>
            <DongleListFilterItem label={"Item 5"} filter={"item5"} count={nbAvailable}
                                  active={filter === 'item5'}/>
            <DongleListFilterItem label={"Item 6"} filter={"item6"} count={nbAvailable}
                                  active={filter === 'item6'}/>
        </Nav>
    )
}