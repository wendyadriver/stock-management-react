import React from "react";
import {Badge, NavItem, NavLink} from "reactstrap";
import Link from "next/link";

type DongleListFilterItemProps = {
    label: string
    filter: string
    count: number
    active: boolean
}

export function DongleListFilterItem(props: DongleListFilterItemProps) {
    return (
        <NavItem className={"bg-light mb-2"}>
            <Link href={"/?filter=" + props.filter}>
                <NavLink className={"d-flex justify-content-between"} active={props.active}
                         href={"/?filter=" + props.filter}>{props.label}{'\u0090'}
                    <Badge pill>
                        {props.count}
                    </Badge>
                </NavLink>
            </Link>
        </NavItem>
    )
}