import React from "react";
import { Collapse, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink } from "reactstrap";

export type HeaderNavProps = {

}

export const HeaderNav: React.FC<HeaderNavProps> = () => {
    return(
        <div>
            <Navbar
                color="light"
                expand="md"
                light
            >
                <NavbarBrand href="/">
                ADriver Stock Manager
                </NavbarBrand>
                <NavbarToggler onClick={function noRefCheck(){}} />
                <Collapse navbar>
                <Nav
                    className="me-auto"
                    navbar
                >
                    <NavItem>
                    <NavLink href="/">
                        Mes dongles
                    </NavLink>
                    </NavItem>
                    <NavItem>
                    <NavLink href="/drivers">
                        Mes drivers
                    </NavLink>
                    </NavItem>
                </Nav>
                </Collapse>
            </Navbar>
            </div>
    );
}