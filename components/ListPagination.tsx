import React, {useEffect, useMemo, useState} from "react";
import {Pagination, PaginationItem, PaginationLink, PaginationProps} from "reactstrap";
import Link from "next/link";
import {useRouter} from "next/router";

export type ListPaginationProps = {
    nbPages: number;
    currentPage?: number;
    onChange: (nextPage: number) => void;
    href?: string
}

type PaginationQuery = {
    page: number
}

export const ListPagination: React.FC<ListPaginationProps & PaginationProps> = ({
                                                                                    nbPages,
                                                                                    currentPage: currentPageParam,
                                                                                    onChange,
                                                                                    href,
                                                                                    ...props
                                                                                }) => {

    const [defaultLocation, setDefaultLocation] = useState<string>();
    const [currentPage, setCurrentPage] = useState(currentPageParam ?? 1);
    const router = useRouter();



    const firstUrl = useMemo(() => {
        if (!defaultLocation)
            return '';
        const url = new URL(href ?? defaultLocation);
        if (url.searchParams.has('page')) {
            url.searchParams.delete('page');
        }
        return url.toString();
    }, [href, currentPage, defaultLocation]);
    const previousUrl = useMemo(() => {
        if (!defaultLocation)
            return '';

        const url = new URL(href ?? defaultLocation);
        if ((currentPage - 1) > 1) {
            url.searchParams.set('page', `${currentPage - 1}`);
        }else{
            url.searchParams.delete('page');
        }
        return url.toString();
    }, [href, currentPage, defaultLocation]);

    const nextUrl = useMemo(() => {
        if (!defaultLocation)
            return '';

        const url = new URL(href ?? defaultLocation);
        if ((currentPage + 1) < nbPages) {
            url.searchParams.set('page', `${currentPage + 1}`);
        }
        return url.toString();
    }, [href, currentPage, nbPages, defaultLocation]);

    const lastUrl = useMemo(() => {
        if (!defaultLocation)
            return '';
        const url = new URL(href ?? defaultLocation);
        if (nbPages > 1) {
            url.searchParams.set('page', `${nbPages}`);
        }
        return url.toString();
    }, [href, currentPage, nbPages, defaultLocation]);

    const pages = useMemo(() => {
        if (!defaultLocation)
            return [];
        return Array.from(new Array(nbPages)).map((item, index) => {
            const url = new URL(href ?? defaultLocation);
            const page = index + 1;
            if (page > 1) {
                url.searchParams.set('page', `${page}`);
            }else{
                url.searchParams.delete('page');
            }
            return {page, href: url.toString(), defaultLocation};
        });
    }, [nbPages, href]);

    useEffect(() => {
        if (!currentPageParam) {
            const nextPage = (router.query as unknown as PaginationQuery).page || 1;
            setCurrentPage(nextPage);
        } else {
            setCurrentPage(currentPageParam);
        }
    }, [router, currentPageParam]);

    useEffect(() => {
        if (typeof window !== 'undefined' && window.location.href) {
            setDefaultLocation(window.location.href);
        }
    }, []);

    if (!defaultLocation)
        return null;

    return (
        <Pagination {...props}>
            <PaginationItem disabled={currentPage === 1}>
                <Link href={firstUrl}>
                    <PaginationLink
                        first
                        href={firstUrl}
                    />
                </Link>
            </PaginationItem>
            <PaginationItem disabled={currentPage === 1}>
                <Link href={previousUrl}>
                    <PaginationLink
                        href={previousUrl}
                        previous
                    />
                </Link>
            </PaginationItem>

            {pages.map((item) => (
                <PaginationItem active={currentPage == item.page}  key={item.page}>
                    <Link href={item.href}>
                        <PaginationLink href={item.href}>
                            {item.page}
                        </PaginationLink>
                    </Link>
                </PaginationItem>
            ))}

            <PaginationItem disabled={currentPage === nbPages}>
                <Link href={nextUrl}>
                    <PaginationLink
                        href={nextUrl}
                        next
                    />
                </Link>
            </PaginationItem>
            <PaginationItem disabled={currentPage === nbPages}>
                <Link href={lastUrl}>
                    <PaginationLink
                        href={lastUrl}
                        last
                    />
                </Link>
            </PaginationItem>
        </Pagination>
    );
}