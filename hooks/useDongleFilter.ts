import {useRouter} from "next/router";
type RouterQuery = {
    filter: string;
}

export function useDongleFilter(){
    const router = useRouter();
    const filter = (router.query as RouterQuery)?.filter ?? 'ongoing';
    return filter;
}