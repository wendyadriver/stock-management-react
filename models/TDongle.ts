export type TDongle = {
    id: number;
    name: string;
    status: string;
}