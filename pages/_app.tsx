import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import type {AppProps} from 'next/app'
import {HeaderNav} from '../components/HeaderNav';
import {DongleReducerProvider} from "../reducers/DongleReducer/DongleReducerProvider";
import {Container} from "reactstrap";
import moment from "moment";
import "moment/locale/fr";

moment.locale('fr');

function MyApp({Component, pageProps}: AppProps) {
    return (
        <DongleReducerProvider>
            <Container fluid={true}>
                <HeaderNav/>
                <Component {...pageProps} />
            </Container>
        </DongleReducerProvider>
    );
}

export default MyApp
