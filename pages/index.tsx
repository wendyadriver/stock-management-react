import type {NextPage} from 'next'
import {config} from '@fortawesome/fontawesome-svg-core'
import 'bootstrap/dist/css/bootstrap.min.css';
import {Button, Col, Row} from 'reactstrap';
import {DongleListFilter} from '../components/DongleListFilter';
import {DongleList} from "../components/DongleList";

config.autoAddCss = false;
const Home: NextPage = () => {
    return (
        <Row>
            <Col sm="3" className="p-2">
                <DongleListFilter/>
            </Col>
            <Col className="p-2">
                <div className={"d-flex justify-content-between"}>
                    <h4>Liste des dongles</h4>
                </div>
                <DongleList/>
            </Col>
        </Row>
    )
}

export default Home
