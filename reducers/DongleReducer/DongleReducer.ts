import {DongleReducerActionsTypes, DongleReducerActionTypes} from './DongleReducerActionsTypes';
import {TDongle} from '../../models/TDongle';

export type DongleReducerState = {
    items: { [filter: string]: TDongle[] };
    loadingItems: boolean;
    loadingItemsError?: Error;
    item?: TDongle;
    loadingItem: boolean;
}


export function DongleReducer(state: DongleReducerState, action: DongleReducerActionsTypes) {
    switch (action.type) {
        case DongleReducerActionTypes.DONGLE_REDUCER_LOAD_ITEMS: {
            return {
                ...state,
                loadingItems: true,
                loadingItemsError: undefined,
                items: {
                    ...state.items,
                    [action.filter]: (action.page === 1 ? [] : state.items[action.filter])
                }
            }
        }

        case DongleReducerActionTypes.DONGLE_REDUCER_LOAD_ITEMS_SUCCESS: {
            return {
                ...state,
                loadingItems: false,
                items: {
                    ...state.items,
                    [action.filter]: action.page === 1 ? action.items: [...state.items[action.filter], ...action.items]
                },
                page: action.page,
                item: (state.item && action.items.find((item) => item.id === state.item?.id)) ?? state.item,
                loadingItemsError: undefined
            }
        }

        case DongleReducerActionTypes.DONGLE_REDUCER_LOAD_ITEMS_ERROR: {
            return {
                ...state,
                loadingItems: false,
                loadingItemsError: action.error
            }
        }
    }

    return state;
}