import {TDongle} from '../../models/TDongle';

export const DongleReducerActionTypes = {
    DONGLE_REDUCER_LOAD_ITEMS: 'DONGLE_REDUCER_LOAD_ITEMS' as const,
    DONGLE_REDUCER_LOAD_ITEMS_SUCCESS: 'DONGLE_REDUCER_LOAD_ITEMS_SUCCESS' as const,
    DONGLE_REDUCER_LOAD_ITEMS_ERROR: 'DONGLE_REDUCER_LOAD_ITEMS_ERROR' as const,
};

export type LoadDongleItems = {
    type: typeof DongleReducerActionTypes.DONGLE_REDUCER_LOAD_ITEMS;
    filter: string;
    page?: number;
    nbPerPage?: number;
}


export type LoadDongleItemsSuccess = {
    type: typeof DongleReducerActionTypes.DONGLE_REDUCER_LOAD_ITEMS_SUCCESS;
    filter: string;
    page: number;
    nbPerPage: number;
    items: TDongle[];
}


export type LoadDongleItemsError = {
    type: typeof DongleReducerActionTypes.DONGLE_REDUCER_LOAD_ITEMS_ERROR;
    error: Error;
}

export type DongleReducerActionsTypes = LoadDongleItems | LoadDongleItemsSuccess | LoadDongleItemsError;