import React, {
    Context,
    createContext,
    Dispatch,
    DispatchWithoutAction,
    useContext,
    useMemo,
    useReducer,
    useRef
} from "react";
import {DongleReducer, DongleReducerState} from "./DongleReducer";
import {DongleReducerActionsTypes} from "./DongleReducerActionsTypes";

export type DongleReducerProviderProps = {
}

const initialDongleReducerValue = {
    items: {
        'ongoing':[{id: 123},{id: 124},{id: 125}] as any,
        'fail':[{id: 221},{id: 222},{id: 223}, {id: 224},{id: 225},{id: 226}] as any,
        'available':[] as any,
    },
    loadingItem: false,
    loadingItems: false
};
type TDongleReducerContext = DongleReducerState & { dispatch: Dispatch<DongleReducerActionsTypes> };
export const DongleReducerContext = createContext<TDongleReducerContext>(initialDongleReducerValue as unknown as TDongleReducerContext);

export const DongleReducerProvider: React.FC<DongleReducerProviderProps> = ({children}) => {
    const [state, dispatch] = useReducer(DongleReducer, initialDongleReducerValue);

    return (
        <DongleReducerContext.Provider
            value={{
                ...state,
                dispatch
            }}>{children}</DongleReducerContext.Provider>
    )
}