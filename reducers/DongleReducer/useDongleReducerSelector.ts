import {DongleReducerState} from "./DongleReducer";
import {useContext, useMemo} from "react";
import {DongleReducerContext} from "./DongleReducerProvider";

export function useDongleReducerSelector<T = any>(selector: (state: DongleReducerState) => T) {
    const state = useContext(DongleReducerContext);
    return useMemo(() => {
        return selector(state);
    }, [state]);
}